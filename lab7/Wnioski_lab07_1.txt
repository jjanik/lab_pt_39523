Wnioski:

Skrypt wy�wietla dwa diagramy, jeden ukazuje prawdopodobie�stwo wyst�pienia danych liter, 
drugi za to wy�witla nam drzewo kodowania Huffmana. 

Wysoko�� drzewa = 3;

Wiadomo��:

abcbcdcddcddd

Prawdopodobie�stwo wyst�powania danych liter:

a -->0.076923
b -->0.15385
c -->0.30769
d -->0.46154

Posortowane drzewo:

'dabc'    'abc'    'd'    'c'    'ab'    'b'    'a'

Posortowane prawdopodobie�stwo:

    1.0000    0.5385    0.4615    0.3077    0.2308    0.1538    0.0769


S�ownik:

'dabc'    ''   
    'abc'     '1'  
    'd'       '0'  
    'c'       '11' 
    'ab'      '10' 
    'b'       '101'
    'a'       '100'

    
    'd'    '0'  
    'c'    '11' 
    'b'    '101'
    'a'    '100'


Wiadomo�� po zakodowaniu:
100101111011101100


Oszacowane przep�ywno�ci kana�u dla danych niezakodowanych: 144
Oszacowane przep�ywno�ci kana�u dla danych zakodowanych: 18

Alfabet Morse�a:

R�ne prawdopodobie�stwa wyst�powania poszczr�lnych liter alfabetu.
W alfabecie Morse'a w�z�y zawieraj� tylko i wy��cznie pojedyncze litery, w moim drzewie wyst�puj� w w�z�ach wi�cej niz jedne litery(np. abc, ab).
Alfabet Morse'a jest nieprefiksowy przez co symbole s� na ga��ziach
a w Huffmana wyst�puj� tylko w li�ciach ko�cowych.  



