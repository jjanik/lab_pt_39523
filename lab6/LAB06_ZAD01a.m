close all
clc


[lr,Fs]=audioread('everything-is-fine.wav');

figure('Name','Caly sygnal');
lr = lr(:,1);

t0=floor(Fs*0.1);
t1=floor(t0*2);
l=lr(t0:t1,1);

dt = 1/Fs;
t = 0:dt:(length(lr)*dt)-dt;
plot(t,lr); xlabel('Seconds'); ylabel('Amplitude');



figure('Name','wid');
L=doDFT(l);
df=Fs/max(size(L))*(1:max(size(L)));
plot(df,abs(L));