xy = imread('lennagrey.png');
xy = rgb2gray(xy);
figure,imshow(xy)

% Dwuwymiarowa dyskretna transformacja kosinusowa
XY = dct2(xy);
figure,imshow(abs(XY))
figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar

% Dwuwymiarowa dyskretna odwrotna transformacja kosinusowa
xy_ = idct2(XY);

figure,imshowpair(xy,xy_,'montage')
title('Oryginalny obraz (lewy), przetworzony (prawy)');