function [h] = h74_encode (data)
% data=[1 0 1 0];
p1=data(1)+data(2)+data(4);
p2=data(1)+data(3)+data(4);
p3=data(2)+data(3)+data(4);

h=[p1, p2,data(1),p3,data(2),data(3),data(4)];
h=mod(h,2);
end
