function [ fY, fU, fV ] = YUV2FDCT(  Y, U, V  )

fY=fdct(y);
fU=fdct(U);
fV=fdct(V);
end