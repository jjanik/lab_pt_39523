function [Y, U, V] = rgb2yuv( R, G, B )
Y= (0.30*double(R))+(0.59*double(G))+(0.11*double(B));
U= 0.56*(double(B)-double(Y))+128;
V= 0.71*(double(R)-double(Y))+128;
end