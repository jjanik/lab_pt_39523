clear all
clear all
clc

fs = 1000; 
A = 1;
f = 10;
fi = 0;
sim_time = 1;

ST = pt(fs);
[t, xt] = ST.getSineSignal(A,f,sim_time,fi)

figure('Name', 'Sinosuida')
plot(t,xt,'k'), title('A=1, f=10HZ, fi=0, sim.time=1s');
xlabel('Czas [s]'), ylabel('amplituda');