classdef pt
    %PT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        
        internal_fs
        
    end
    
    methods
        
        function [ obj ] = pt( fs )
         %konstruktor
         if nargin > 0
             %Sprawdza czy byl podany argument
            if isnumeric(fs)
               obj.internal_fs = fs;
               %Jesli tak, to zmiennej wewnetrznej fs przypisuje podajna
               %wartosc argumentu
            else
               error('Bad input data')
               %w przeciwnym wypadku blad
            end
         end
         %...
      end
      
      function [ xt ] = sinewave(obj,A,f,t,fi)
         %metoda sinewave - odpowiednik DSP Sine Wave
         xt = A * sin(2*pi*f*t+fi);
      end
      
      function [t,x] = getSineSignal(obj,A,f,sim_time,fi)
          x = [];
          t = [];
          %zainicjowanie pustych wektorow
          for i=0:(sim_time/obj.internal_fs):sim_time
              %i dzieli czas od zera, na czesci o wielkosci 1/fs, do
              %podanego czasu symulacji
              
              t = [t,i];
              x = [x, sinewave(obj, A, f, i, fi)];
              %kazdy nowy "t" o "x" poszerza wektor o zadane zmienne
              
          end
      
    end
    
    end
end

