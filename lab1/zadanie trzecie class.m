classdef Untitled
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    internal_fs
    end
    
    methods
        function [ obj ] = pt ( fs )
        %konstruktor
        if nargin > 0
        %argument zosta� podany?
        if isnumeric(fs)
            obj.internal_fs = fs;
        % tak = do fs przypisuje warto�� argumentu    
        else
            error('Bad input data')
        % nie = b��d
        end
    end
        end

        function [xt ] = sinewave (obj,A,f,t,fi)
            %sinewave
            xt = A * sin(2*pi*f*t+fi);
        end
        
        function [t,x] = getSineSignal (obj,A,f,sim_time,fi)
            
         x = [];
         t = [];
         
         %puste wektory
         
         for i-0: (sim_time/obj.internal_fs) :sim_time
             %i dzieili czas od zera
             
             t = [t,i];
             x = [x, sinewave(obj, A, f, i, fi)];
         end
        end
    end
end
 

