close all
clear all
clc

%% dane do ustawienia
%do testow dane pozostaw pocz�tkowo jak w oryginale
%nast�pnie zmie� dane na swj nr album zapisany binarnie
data = [1 0 1 0 1 1 0 0];%strumie� danych

%Bod (ang. Baud), typowe warto�ci: 1200/2400/4800/9600
bod=10;

%% zmienne pomocnicze
sim_fs=bod*100; %cz�stotliwo�c probkowania dla symulacji 100 wi�ksza od Bod
%w trakcie jednej sek wystepuje 1000 probek
sp_clk=sim_fs/bod; %sampli na cykl zegara
%na jeden cykl zegara przypada 100 sampli
N = length(data); %ilo�� bitow w danych wej�ciowych
%tutej 8
sp_d=N*sp_clk; %sampli na ilo�� danych
%tutej 800
t = 0:(1/sim_fs):((sp_d/sim_fs)-(1/sim_fs)); %czas trwania sygnalu
% od 0; krok 1/1000; do 7/10
empty = zeros(1,length(t));%pomocniczy pusty wektor 

CLK     = clkGenerator(sp_clk,sp_d);%sygnal zegara

%% zadanie wa�ciwe
%strumie� po modulacji
%gdy napiszesz wasn� funkcj� modulatora zamie� empty na wa�ciw� nazw� funkcji
TTL     = ttlGenerator(CLK,data);
ASK     = askGenerator(TTL,bod,sim_fs);
FSK     = fskGenerator(TTL,bod*2,bod,sim_fs);
PSK     = pskGenerator(TTL,bod,sim_fs);%dok�adnie BPSK
DASK     = askDegenerator(ASK,bod,sim_fs);

%% pomocnicza funkcja rysuj�ca
plotSignalModulations(t, CLK, TTL, ASK, FSK, PSK);
