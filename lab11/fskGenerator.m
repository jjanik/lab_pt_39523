function [FSK] = fskGenerator(TTL,f0,f1,fs)
%TTL - wektor pr�bek sygna�u TTL wygenerowanego na podstawie strumienia binarnego
%f0 - cz�stotliwo�� I sygna�u no�nego
%f1 - cz�stotliwo�� II sygna�u no�nego
%fs - cz�stotliwo�� pr�bkowania

for  n=1:1:length(TTL)
    if TTL(n)==1
    FSK(n) = 1.0*sin(2*pi*f1*(n/fs)+0);
    end
    if TTL(n)==0
    FSK(n) = 1.0*sin(2*pi*f0*(n/fs)+0);
    end
    
end
%tu wpisz sw�j kod :)
end

